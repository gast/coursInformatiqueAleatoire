\documentclass[twocolumn]{article}

\usepackage{amsmath,amsfonts,amsthm}
\newtheorem{theorem}{Theorem}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage[margin=1cm]{geometry}

\newcommand\Proba[1]{\mathbb{P}(#1)}
\newcommand\esp[1]{\mathbb{E}(#1)}
\newcommand\var[1]{\mathrm{var}(#1)}
\newcommand\calX{\mathcal{X}}
\newcommand\R{\mathbb{R}}

\title{Mémo -- Licence L3 Informatique\\
  UEX Informatique et aléatoire}
\author{Nicolas Gast}

\begin{document}
\twocolumn[
\begin{center}
  \Large Mémo -- Licence L3 Informatique\\
  UEX Informatique et aléatoire\\
  \url{nicolas.gast@inria.fr}
\end{center}]

\thispagestyle{empty}


\section{Variables aléatoires et convergence}

% Dans cette partie du cours, on considère des suites de variables
% aléatoires où chaque variable $X_n$ prend ses valeurs dans un espace
% discret $\calX$ ou dans $\R$.
% La loi d'une variable aléatoire discrète $X$ à valeur dans $\calX$ est
% la fonction $p:\calX\to[0,1]$ telle que:
% \begin{equation*}
%   \Proba{X=x} = p_x.
% \end{equation*}

% La fonction de répartition d'une variable réelle $X$ est la fonction
% $F:\R\to[0,1]$ définie par
% \begin{equation*}
%   F(x) = \Proba{X \le x}
% \end{equation*}

On dit qu'une suite de variables aléatoires $(X_n)_n$ converge vers
une variable $X$ \emph{presque sûrement} si
\begin{equation*}
  \Proba{\lim_{n\to\infty}X_n=X} = 1
\end{equation*}

On dit qu'une suite de variables aléatoires discrètes $(X_n)_n$
converge en loi vers une variable $X$ si pour tout $x\in\calX$, on a:
\begin{equation*}
  \lim_{n\to\infty}\Proba{X_n=x} = \Proba{X=x}. 
\end{equation*}

\begin{theorem}[Loi des grands nombres]
  Soit $X_n$ une suite de variables \emph{i.i.d.} telle que
  $\esp{|X_1|}<\infty$. Alors, 
  \begin{equation*}
    \lim_{n\to\infty} \frac1n\sum_{k=1}^n X_k = \esp{X_1}
    \qquad\text{ (presque sûrement)}
  \end{equation*}
\end{theorem}

\section{Chaînes de Markov et Optimisation}

Une chaîne de Markov est une suite de variables aléatoires $(X_n)$ qui
satisfait la propriété de Markov, c'est-à-dire:
\begin{equation*}
  \Proba{X_{n+1}=j\mid X_n=i,X_{n-1}\dots X_1}=\Proba{X_{n+1}=j\mid
    X_n=i}
\end{equation*}
Une chaîne de Markov homogène en temps est décrite par sa matrice de
transition $P$, où $P_{i,j} := \Proba{X_{n+1}=j\mid X_n=i}$.

Soit $\pi_n(j)=\Proba{X_n=j}$. Le vecteur de probabilité $\pi_{n+1}$
satisfait $\pi_{n+1}=\pi_nP$. Autrement dit:
\begin{equation*}
  \pi_{n+1}(j) = \sum_{i}\pi_n(i)P_{ij} = (\pi P)_j
\end{equation*}

\subsection{Temps moyen d'atteinte}

Soit $\tau_{i,x} := \inf\{n : X_n=x \mid X_0=i\}$. On a:
\begin{align*}
  \esp{\tau_{i,x}}
  &= \left\{
    \begin{array}{ll}
      0 & \text{ si $i=x$}\\
      1+\sum_j P_{ij} \esp{\tau_{j,x}} & \text{ si $i\ne x$},
    \end{array}
                                         \right.\\
  \Proba{\tau_{i,x}<\tau_{i,y}}&=\left\{
  \begin{array}{ll}
    1 & \text{ si $i=x$}\\
    0 & \text{ si $i=y$}\\
    \sum_j P_{ij} \Proba{\tau_{j,x}<\tau_{j,y}} & \text{ si $i\not\in\{x,y\}$},
  \end{array}
                                                  \right.
\end{align*}

\subsection{Equation de Bellman et optimisation}

Les équations ci dessus peuvent être vu comme des cas particuliers de
l'équation de Bellman. On suppose qu'à chaque instant on paie un coût
$c(i)$ avant de faire une transition (sauf dans des états finaux dans
lequels on paie $f(i)$ et on s'arrete). En notant $V(i)$ la valeur
depuis un état $i$ et $Q(i,a)$ la valeur de prendre une action $a$
dans l'état $i$, on a:
\begin{align*}
  V(i) &= \left\{
         \begin{array}{ll}
           c(i)+\sum_{j}  \max_a Q(i,a)&\text{ si $i$ n'est pas
                                         final}\\
           f(i)&\text{ si $i$ est final}
         \end{array}
                 \right.\\
  Q(i,a) &= \sum_j P_{j|i,a}V(j),
\end{align*}
où $P_{j|i,a}$ est la probabilité que le prochain état soit $j$
sachant que l'on vient de prendre l'action $a$ dans l'état $i$.

% Cette équation peut être vu comme un cas particulier de l'équation de
% Bellman:
% \begin{equation*}
%   V_n (i) = f(i) +  \max_{a} \sum_{j} P_{i,j}(a) V_{n+1}(j). 
% \end{equation*}
% avec $V_N(i) = f(i)$, où
% $V_n(i) = \max \{\esp{\sum_{k=n}^N f(X_k) \mid X_n=i}$.

\section{Génération de variables aléatoires}

\subsection{Méthode de la fonction inverse}

\begin{theorem}
  Soit $F$ la fonction de répartition d'une variable aléatoire réelle
  $X$. Soit $F^{-1}$ l'inverse de $F$, définie par
  $F^{-1}(p) = \sup\{x : F(x)\le p\}$.  Soit $U$ une variable
  aléatoire uniformément entre $0$ et $1$. Alors, la variable
  $F^{-1}(U)$ est distribuée selon $X$.
\end{theorem}

\subsection{Méthode du rejet}

On dispose d'un générateur uniforme d'un objet dans un ensemble $B$ et
une façon de tester si un élément de $B$ est dans $A\subset B$. Alors,
l'algorithme suivant génère une variable uniformément distribuée sur
$A$: 
\begin{verbatim}
Répéter: 
  Générer X uniformément sur B
Tant que X n'est pas dans A
\end{verbatim}

Soit $p$ la probabilité qu'un $X$ généré uniformément sur $B$ soit
dans $A$. Le nombre moyen de boucles que fait l'algorithme de rejet
est $1/p$.

\section{Distribution stationnaire}
\subsection{Classification des états}

Un ensemble d'états $C$ est une classe d'irréductibilité si
\begin{itemize}
\item Pour tout $i,j\in C$, il existe un chemin de probabilité
  positive de $i$ à $j$. 
\item Pour tout $i\in C$, $j\ne C$, il n'existe pas de chemin de
  probabilité positive de $i$ à $j$.
\end{itemize}
Une classe irréductible est apériodique si le PGCD de la longueur des
cycles est égal à $1$.  Une chaîne est irréductible si elle n'a qu'une
composante irréductible.


\subsection{Distribution stationnaire}

On dit qu'une distribution de probabilité $\pi$ est stationnaire si
$\pi = \pi P$.

\begin{theorem}
  \begin{enumerate}
  \item Une chaîne de Markov irréductible (à espace d'état fini) a une
    unique distribution stationnaire.
  \item Si en plus la chaîne est apériodique, alors pour toute
    distribution initiale: $\pi_0$, $\lim_{n\to\infty}\pi_n = \pi$.
  \end{enumerate}
\end{theorem}

% \begin{theorem}
%   Soit $X_n$ une chaîne de Markov irréductible (à espace d'état
%   fini). Alors: pour toute fonction réelle $f:\calX\to\R$:  
%   \begin{equation*}
%     \lim_{n\to\infty} \frac1n\sum_{k=1}^n f(X_k)  = \sum_if(i) \pi_i. 
%   \end{equation*}
% \end{theorem}

\subsection{Réversibilité}

On dit qu'une chaîne $X_n$ est réversible par rapport à une
distribution $\pi$ si pour tous états $i,j$:
$\pi_i P_{ij} = \pi_j P_{ji}$
\begin{theorem}
  Si une telle distribution existe, alors $\pi$ est une distribution
  stationnaire.
\end{theorem}

% \section{}
\end{document}
