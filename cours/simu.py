def mm1(x_init,p,T=1000):
    X = [x_init]*T;
    for i in range(1,T):
        if ( rand() < p):X[i] = (X[i-1]+1)
        else: X[i] = max(X[i-1]-1,0)
    return X;

clf();
plot(mm1(20,.4));
show();

M = [ mm1(20,.4) for i in range(0,10000)]
V = [ [-1]*10000 for i in range(0,1000)];
for i in range(0,1000):
    for j in range(0,10000):
        V[i][j] = M[j][i];

P = [ [0]*1000 for i in range(0,100)];
for i in range(0,1000):
    for j in range(0,10000):
        P[V[i][j]][i] += 1;

        
X = [20];
for i in range(0,1000):
    if ( rand() < .6):X.append(X[-1]+1)
    else: X.append(max(X[-1]-1,0))



clf();
plot(X);
show();


clf();
for t in range(0,20):
    X = [20];
    for i in range(0,1000):
        if ( rand() < .6):X.append(X[-1]+1)
        else: X.append(max(X[-1]-1,0))
    plot(X);
show();



N=1;
clf();
seed(1);
for i in range(0,N):
    X = [.5];
    for n in range(1,1000):
        if (rand() < X[-1]):
            X.append((X[-1]*n+1)/(n+1));
        else:
            X.append(X[-1]*n/(n+1));
    plot(X);   

# Ruine:

clf();
for i in range(0,20):
    X = [0];
    y = 1;
    for n in range(0,20):
        if (X[-1] <= 0):
            if (rand() < .2):
                X.append(X[-1]+y);
            else:
                X.append(X[-1]-y);
        else:
            X.append(X[-1]);
        y = 2*y;
    plot(X);

show();


lamb = 2;
Poiss=[]
for k in range(0,10):
    Poiss.append(lamb**k*exp(-lamb)/math.factorial(k));

clf();
for i in range(1,3):
    n=10**i;
    X=[];
    p=lamb/n
    for k in range(0,10):
        X.append(scipy.special.binom(n,k)*p**k*(1-p)**(n-k));
        plot(X,'o');
plot(Poiss);
show();



## Convergence vers une loi normale:

T = 100000;
for i in range(0,4):
    X=[-1]*100000;
    Y = rand(100000)
    for i in range(0,100000):
        if (Y[i] < .5):
            X[i] = 1;
    
    plot (cumsum(X)/[ sqrt(i) for i in range(0,100000)])
    

## Pareto
from scipy.stats import pareto
alpha = 1.2;
T = 100000;
clf();
for i in range(0,5):
    plot(cumsum(pareto.rvs(alpha,size=T))/range(1,T+1))
show();

moy = pareto.stats(alpha, moments='m');
moy2 = sum(pareto.rvs(alpha,size=T*100))/(100*T);
print(moy, moy2)

T = 1000;
nb = 10000;
Y = [0] * nb;
for i in range(0,nb):
    Y[i] = sum(pareto.rvs(alpha,size=T)-moy)/sqrt(T);

hist(Y,bins=100, range=(-200,400));

T = 10000;
nb = 10000;
Y = [0] * nb;
for i in range(0,nb):
    Y[i] = sum(pareto.rvs(alpha,size=T)-moy)/sqrt(T);

hist(Y,bins=100, range=(-200,400));

alpha = .5;
T = 100000;
clf();
for i in range(0,5):
    plot(cumsum(pareto.rvs(alpha,size=T))/range(1,T+1))
show();

